package com.sky.mapper;

import com.sky.dto.GoodsSalesDTO;
import org.apache.ibatis.annotations.Mapper;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Mapper
public interface OrderMapper {

    Double sumByMap(Map map);

    Integer countByMap(Map map);

    List<GoodsSalesDTO> top10(HashMap<String, Object> map);

}
